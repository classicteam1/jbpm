package com.sample;

import org.jbpm.test.JbpmJUnitBaseTestCase;
import org.junit.Test;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.api.runtime.manager.RuntimeManager;
import org.kie.api.runtime.process.ProcessInstance;

import bitronix.tm.resource.jdbc.PoolingDataSource;

/**
 * This is a sample file to test a process.
 */
public class ProcessTest extends JbpmJUnitBaseTestCase {

	@Test
	public void testProcess() {
		setupDataSource();
		RuntimeManager manager = createRuntimeManager("com/sample/sample3.bpmn");
		RuntimeEngine engine = getRuntimeEngine(null);
		KieSession ksession = engine.getKieSession();
		
		ProcessInstance processInstance = ksession.startProcess("com.sample.bpmn.hello3");
		// check whether the process instance has completed successfully
		assertProcessInstanceCompleted(processInstance.getId(), ksession);
		assertNodeTriggered(processInstance.getId(), "Hello");
		
		manager.disposeRuntimeEngine(engine);
		manager.close();
		
	}

	

	public static PoolingDataSource setupDataSource() {
		PoolingDataSource ds1 = new PoolingDataSource();
		ds1.setUniqueName("jdbc/jbpm-ds");
        ds1.setClassName("oracle.jdbc.xa.client.OracleXADataSource");
        ds1.setMaxPoolSize( 5 );
        //ds1.setMinPoolSize(1);
        ds1.setAllowLocalTransactions( true );
        
        ds1.getDriverProperties().put("user","sa");
        ds1.getDriverProperties().put("password","sa");
        ds1.getDriverProperties().put("URL","jdbc:oracle:thin:@172.25.180.112:1521:CLSDEV11");
        ds1.init();
        return ds1;
		
	}
}